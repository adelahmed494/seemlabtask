<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_masters', function (Blueprint $table) {
            $table->id();
            $table->string('order_no');
            $table->double('delivery_service_value', 5, 2)->default('0');
            $table->enum('type',array('dine-in','delivery','takeaway'))->nullable(false);
            $table->double('total_price', 5, 2);
            $table->bigInteger('user_id')->unsigned()->index()->nullable(false);
            $table->bigInteger('waiter_id')->unsigned()->index()->nullable();
            $table->bigInteger('customer_id')->unsigned()->index()->nullable();
            $table->bigInteger('table_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('waiter_id')->references('id')->on('users');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('table_id')->references('id')->on('tables');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_masters');
    }
};
