<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_details', function (Blueprint $table) {
            $table->id();
            $table->double('qty', 5, 2);
            $table->double('unit_price', 5, 2);
            $table->double('total', 5, 2);
            $table->bigInteger('item_id')->unsigned()->index()->nullable(false);
            $table->bigInteger('order_master_id')->unsigned()->index()->nullable(false);
            $table->foreign('item_id')->references('id')->on('items')->nullable(false);
            $table->foreign('order_master_id')->references('id')->on('orders_masters')->nullable(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_details');
    }
};
