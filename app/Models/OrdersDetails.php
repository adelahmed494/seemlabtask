<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Itemable;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersDetails extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Itemable;

    protected $fillable = ['qty', 'unit_price','total','item_id','order_master_id'];

    public static $createRules = [
        'qty' => 'required|numeric',
        'unit_price' => 'required|numeric',
        'total' => 'required|numeric',
        'item_id' => 'required|integer|exists:items,id'
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];
}
