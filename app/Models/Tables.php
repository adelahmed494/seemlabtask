<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tables extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['no', 'location'];

    public static $createRules = [
        'no' => 'required|string',
        'location' => 'nullable|string'
    ];

    public static $updateRules = [
        'no' => 'required|string',
        'location' => 'nullable|string'
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];
}
