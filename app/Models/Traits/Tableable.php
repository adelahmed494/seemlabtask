<?php
namespace App\Models\Traits;

Trait Tableable
{
    public function table(){
        return $this->belongsTo('App\Models\Tables', 'table_id', 'id');
    }
}
