<?php
namespace App\Models\Traits;

Trait Itemable
{
    public function item(){
        return $this->belongsTo('App\Models\Items', 'item_id', 'id');
    }
}
