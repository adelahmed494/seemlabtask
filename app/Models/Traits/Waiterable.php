<?php
namespace App\Models\Traits;

Trait Waiterable
{
    public function waiter(){
        return $this->belongsTo('App\Models\User', 'waiter_id', 'id');
    }
}
