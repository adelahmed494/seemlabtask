<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name', 'mobile','address'];

    public static $createRules = [
        'name' => 'required|string',
        'mobile' => 'required|string',
        'address' => 'nullable|string'
    ];

    public static $updateRules = [
        'name' => 'required|string',
        'address' => 'nullable|string',
        'mobile' => 'required|numeric'
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];
}
