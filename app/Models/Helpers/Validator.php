<?php


namespace App\Helpers;

use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Support\Facades\Validator as BaseValidator;

class Validator extends BaseValidator
{

    public function validateRequest(array $data, array $rules, array $messages=[])
    {
        /*var_dump($data);
        die();*/
        $validator = Validator::make($data, $rules, $messages);
        
        if ($validator->fails()) {
            throw new ValidationHttpException($validator->errors());
        }

        return true;
    }
}
