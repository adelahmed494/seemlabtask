<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Items extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['name', 'description','price'];

    public static $createRules = [
        'name' => 'required|string',
        'description' => 'nullable|string',
        'price' => 'required|numeric'
    ];

    public static $updateRules = [
        'name' => 'required|string',
        'description' => 'nullable|string',
        'price' => 'required|numeric'
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];

}
