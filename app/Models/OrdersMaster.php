<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Userable;
use App\Models\Traits\Waiterable;
use App\Models\Traits\Customerable;
use App\Models\Traits\Tableable;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersMaster extends Model
{
    use HasFactory;
    use SoftDeletes;

    use Userable;
    use Waiterable;
    use Customerable;
    use Tableable;

    protected $fillable = ['delivery_service_value', 'type','total_price','user_id','waiter_id','customer_id','table_id','order_no'];

    public static $createRules = [
        'type' => 'required|string|in:dine-in,delivery,takeaway',
        'user_id' => 'required|integer|exists:users,id',
        'waiter_id' => 'nullable|integer|exists:users,id',
        'customer_id' => 'nullable|integer|exists:customers,id',
        'table_id' => 'nullable|integer|exists:tables,id',
        'details' => 'required',
        
    ];

    public static $updateRules = [
        'type' => 'required|string|in:dine-in,delivery,takeaway',
        'user_id' => 'required|integer|exists:users,id',
        'waiter_id' => 'nullable|integer|exists:users,id',
        'customer_id' => 'nullable|integer|exists:customers,id',
        'table_id' => 'nullable|integer|exists:tables,id',
        'price' => 'required|numeric'
    ];

    public static $listRules = [
        'per_page'   => 'required|numeric',
        'page'   => 'required|numeric'
    ];

    public function details(){
        return $this->hasMany('App\Models\OrdersDetails', 'order_master_id', 'id');
    }

}
