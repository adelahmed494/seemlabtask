<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Items;
use App\Http\Resources\ItemsResource;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $this->ValidateRequest($request->all(),Items::$listRules);
        $items = Items::paginate($request->per_page);
        return ItemsResource::collection($items);
    }

    public function store(Request $request)
    {
        $this->ValidateRequest($request->all(),Items::$createRules);
        
            $item = Items::create([
                'name' => $request->name,
                'description' => isset($request->description) ? $request->description : null,
                'price' => $request->price,
            ]);
    
            $data = [
                'status' => 'success',
                'message' => 'Item Saved successfully',
                'data' => new ItemsResource($item)
            ];
    
            return response()->json($data, 201);
    }

    public function show($id)
    {
        $item = Items::find($id);
        if($item){
            return new ItemsResource($item);
        }else{
            $this->errorMessage('Resource not found');
        }
    }

    public function update(Request $request, $id)
    {
        $this->ValidateRequest($request->all(),Items::$updateRules);

        $item = Items::find($id);

        $item->name = $request->name;
        $item->price = $request->price;
        $item->description = isset($request->description) ? $request->description : $item->description;
        $item->save();

        $data = [
            'status' => 'success',
            'message' => 'Item updated successfully',
            'data' => new ItemsResource($item)
        ];

        return response()->json($data, 201);

    }

    public function destroy($id)
    {
        $item = Items::find($id);
        $item->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Item deleted successfully',
            'item' => new ItemsResource($item),
        ]);
    }


}
