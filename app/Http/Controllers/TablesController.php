<?php

namespace App\Http\Controllers;

use App\Models\Tables;
use Illuminate\Http\Request;
use App\Http\Resources\TablesResource;

class TablesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $this->ValidateRequest($request->all(),Tables::$listRules);
        $table = Tables::paginate($request->per_page);
        return TablesResource::collection($table);
    }

    public function store(Request $request)
    {
        $this->ValidateRequest($request->all(),Tables::$createRules);

        $table = Tables::create([
            'no' => $request->no,
            'location' => isset($request->location) ? $request->location : null
        ]);

        $data = [
            'status' => 'success',
            'message' => 'Table Saved successfully',
            'data' => new TablesResource($table)
        ];

        return response()->json($data, 201);

    }

    public function show($id)
    {
        $table = Tables::find($id);
        if($table){
            return new TablesResource($table);
        }else{
            $this->errorMessage('Resource not found');
        }
    }

    public function update(Request $request, $id)
    {
        $this->ValidateRequest($request->all(),Tables::$updateRules);

        $table = Tables::find($id);

        $table->no = $request->no;
        $table->location = isset($request->location) ? $request->location : $table->location;
        $table->save();

        $data = [
            'status' => 'success',
            'message' => 'Table updated successfully',
            'data' => new TablesResource($table)
        ];

        return response()->json($data, 201);

    }

    public function destroy($id)
    {
        $table = Tables::find($id);
        $table->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Table deleted successfully',
            'data' => new TablesResource($table)
        ]);
    }
}
