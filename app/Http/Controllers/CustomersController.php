<?php

namespace App\Http\Controllers;

use App\Models\Customers;
use Illuminate\Http\Request;
use App\Http\Resources\CustomersResource;

class CustomersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $this->ValidateRequest($request->all(),Customers::$listRules);
        $customers = Customers::paginate($request->per_page);
        return CustomersResource::collection($customers);
    }

    public function store(Request $request)
    {
        $this->ValidateRequest($request->all(),Customers::$createRules);
        $customer = Customers::create([
            'name' => $request->name,
            'address' => isset($request->address) ? $request->address : null,
            'mobile' => $request->mobile,
        ]);

        $data = [
            'status' => 'success',
            'message' => 'Customer Saved successfully',
            'data' => new CustomersResource($customer)
        ];

        return response()->json($data, 201);

    }

    public function show($id)
    {
        $customer = Customers::find($id);
        if($customer){
            return new CustomersResource($customer);
        }else{
            $this->errorMessage('Resource not found');
        }
        
    }

    public function update(Request $request, $id)
    {
        $this->ValidateRequest($request->all(),Customers::$updateRules);

        $customer = Customers::find($id);

        $customer->name = $request->name;
        $customer->mobile = $request->mobile;
        $customer->address = isset($request->address) ? $request->address : $customer->address;
        $customer->save();

        $data = [
            'status' => 'success',
            'message' => 'Customer updated successfully',
            'data' => new CustomersResource($customer)
        ];

        return response()->json($data, 201);

    }

    public function destroy($id)
    {
        $customer = Customers::find($id);
        $customer->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Customer deleted successfully',
            'Customer' => new CustomersResource($customer),
        ]);
    }

}
