<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProblemSolvingController extends Controller
{
    public function question1(Request $request)
    {
        $this->ValidateRequest($request->all(),['arr_values' => 'required|array']);

        $arr_data = $request->arr_values;
        sort($arr_data);

        //Remove (-) values
        $arr_data = array_filter($arr_data, function($v,$k) {
            return $v > 0;
        },ARRAY_FILTER_USE_BOTH);

        //remove repeated numbers and index array to start from 0
        $arr_data = array_values(array_unique($arr_data));

        $arr_length = sizeof($arr_data);

        // handle case if array empty return 1 as first positive number
        // if has one element return the next number
        if($arr_length == 0){
            return 1;
        }else if($arr_length == 1){
            return $arr_data[0] + 1;
        }

        for($i=0;$i<$arr_length-1;$i++){
            if($arr_data[$i] +1 != $arr_data[$i+1]){
                return $arr_data[$i] +1;
            }
        }

        return $arr_data[$arr_length-1] + 1;
    }
    
    public function question2(Request $request)
    {
        $this->ValidateRequest($request->all(),['start_number' => 'required|integer','end_number' => 'required|integer']);

        if($request->start_number > $request->end_number){
            $this->errorMessage('Start number sholuld be less than end number'); 
        }

        $count = 0;
        for($i=$request->start_number;$i<=$request->end_number;$i++){
            if(str_contains($i,'5')){
                continue;
            }
            $count++;
        }
        return $count;

    }

    public function question3(Request $request)
    {
        $this->ValidateRequest($request->all(),["input_string" => "required|regex:/^[A-Z_\-]*$/"]);

        $indexArr =["A"=>1,"B"=>2,"C"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,
        "L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>26];

        $input = strrev($request->input_string);
        $outbut = 0;

        for($i=0;$i<strlen($input);$i++){
            if($i == 0){
                $outbut += $indexArr[$input[$i]];
            }else{
                $outbut += (pow(26,$i) * $indexArr[$input[$i]]);
            }
        }

        return $outbut;

    }

    public function question4(Request $request)
    {
        $this->ValidateRequest($request->all(),['Q' => 'required|array']);
        $arr_size = sizeof($request->Q);
        $arr_data = $request->Q;

        $output = array();

        for($i=0;$i<$arr_size;$i++){
            $count = 0;
            $j = $arr_data[$i];
            while($j > 0){
                if($j %2 == 0){
                    $j = $j / 2;
                }else{
                    $j--;
                }
                $count++;
            }

            $output[] = $count;
        }

        return response()->json($output, 201);
    }

}
