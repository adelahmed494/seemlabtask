<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrdersMaster;
use App\Models\OrdersDetails;
use App\Http\Resources\OrdersMasterResource;

class OrdersMasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $this->ValidateRequest($request->all(),OrdersMaster::$listRules);
        $orders = OrdersMaster::paginate($request->per_page);
        return OrdersMasterResource::collection($orders);
    }

    public function show($id)
    {
        $order = OrdersMaster::find($id);
        if($order){
            return new OrdersMasterResource($order);
        }else{
            $this->errorMessage('Resource not found');
        }
        
    }

      public function store(Request $request){

       $this->ValidateRequest($request->all(),OrdersMaster::$createRules);

       if($request->type == 'delivery' && (!isset($request->delivery_service_value) || empty($request->delivery_service_value) 
                                        || !isset($request->customer_id) || empty($request->customer_id))){
            $this->errorMessage('Customer and Deliver fees required with type delivery'); 
        }
        
        if($request->type == 'dine-in' && (!isset($request->delivery_service_value) || empty($request->delivery_service_value) 
                                        || !isset($request->table_id) || empty($request->table_id)
                                        || !isset($request->waiter_id) || empty($request->waiter_id))){
            $this->errorMessage('table number, service charge and waiter name required with type dine-in'); 
        }

        $details_data = array();

        $latestOrder = OrdersMaster::orderBy('created_at','DESC')->first();

        if($latestOrder){
            $orderNo = str_pad((int)$latestOrder->order_no + 1, 8, "0", STR_PAD_LEFT);
        }else{
            $orderNo = str_pad(0 + 1, 8, "0", STR_PAD_LEFT);
        }

        $request['order_no'] = $orderNo;
        $total_price = 0;

        foreach($request['details'] as $details){
            $data = array();
 
            $data['item_id'] =  $details['item_id'];
            $data['qty'] =  $details['qty'];
            $data['unit_price'] =  $details['unit_price'];
            $data['total'] =  $details['qty'] * $details['unit_price'];
            $total_price += $data['total'];
            array_push($details_data,$data); 
        }

        $this->ValidateRequest($data,OrdersDetails::$createRules);
        
        $master = OrdersMaster::create([
            'type' => $request->type,
            'order_no' => $request->order_no,
            'user_id' => $request->user_id,
            'total_price' => $total_price,
            'delivery_service_value' => isset($request->delivery_service_value) ? $request->delivery_service_value : 0,
            'waiter_id' => isset($request->waiter_id) ? $request->waiter_ids : null,
            'customer_id' => isset($request->customer_id) ? $request->customer_id : null,
            'table_id' => isset($request->table_id) ? $request->table_id : null
            
        ]);

        if(isset($master->id)){
            foreach($details_data as $details){
                $details['order_master_id'] = $master->id;
                $orderDetailsData = OrdersDetails::create($details);
            }
        }

       $data = [
            'status' => 'success',
            'message' => 'Order Saved successfully',
            'data' => new OrdersMasterResource($master)
        ];

        return response()->json($data, 201);

    }

    
}
