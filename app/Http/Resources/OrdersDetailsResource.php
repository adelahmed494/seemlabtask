<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ItemsResource;

class OrdersDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'qty'                      => $this->qty,
            'unit_price'                      => $this->unit_price,
            'total'                      => $this->total,
            'items'                      => isset($this->item) ? ItemsResource::make($this->item) : null,
        ];

        //return parent::toArray($request);
    }
}
