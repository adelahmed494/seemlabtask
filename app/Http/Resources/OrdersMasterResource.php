<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CustomersResource;
use App\Http\Resources\TablesResource;
use App\Http\Resources\OrdersDetailsResource;
use App\Http\Resources\UsersResource;
class OrdersMasterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return [
            'id'                        => $this->id,
            'order_no'                      => $this->order_no,
            'type'                      => $this->type,
            'delivery_service_value'                      => $this->delivery_service_value,
            'total_price'                      => $this->total_price,
            'total_after_fees'                      => $this->total_price + $this->delivery_service_value,
            'created_at'                      => $this->created_at,
            'created_by'                      => isset($this->user) ? UsersResource::make($this->user) : null,
            'cutomer'                      => isset($this->customer) ? CustomersResource::make($this->customer) : null,
            'table'                      => isset($this->table) ? TablesResource::make($this->table) : null,
            'waiter'                      => isset($this->waiter) ? UsersResource::make($this->waiter) : null,
            'details'                      => isset($this->details) ? OrdersDetailsResource::collection($this->details) : null,
        ];

        //return parent::toArray($request);
    }
}
