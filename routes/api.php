<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\TablesController;
use App\Http\Controllers\OrdersMasterController;
use App\Http\Controllers\ProblemSolvingController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
    Route::post('refresh', 'refresh');

});

Route::controller(ItemsController::class)->group(function () {
    Route::get('items', 'index');
    Route::post('item', 'store');
    Route::get('item/{id}', 'show');
    Route::put('item/{id}', 'update');
    Route::delete('item/{id}', 'destroy');
}); 

Route::controller(CustomersController::class)->group(function () {
    Route::get('customers', 'index');
    Route::post('customer', 'store');
    Route::get('customer/{id}', 'show');
    Route::put('customer/{id}', 'update');
    Route::delete('customer/{id}', 'destroy');
}); 

Route::controller(TablesController::class)->group(function () {
    Route::get('tables', 'index');
    Route::post('table', 'store');
    Route::get('table/{id}', 'show');
    Route::put('table/{id}', 'update');
    Route::delete('table/{id}', 'destroy');
}); 

Route::controller(OrdersMasterController::class)->group(function () {
    Route::get('orders', 'index');
    Route::post('order', 'store');
    Route::get('order/{id}', 'show');
    Route::put('order/{id}', 'update');
    Route::delete('order/{id}', 'destroy');
}); 

Route::controller(ProblemSolvingController::class)->group(function () {
    Route::post('question/1', 'question1');
    Route::get('question/2', 'question2');
    Route::get('question/3', 'question3');
    Route::post('question/4', 'question4');
}); 

